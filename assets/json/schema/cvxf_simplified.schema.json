{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Resume",
  "type": "object",
  "properties": {
    "personalInformation": {
      "$comment": "Personal Information",
      "title": "Personal Information",
      "description": "Stores the candidate's personal information",
      "type": "object",
      "properties": {
        "fullName": {
          "type": "string"
        },
        "email": {
          "type": "string",
          "format": "email"
        },
        "phone": {
          "type": "string"
        },
        "address": {
          "type": "string"
        },
        "linkedInProfile": {
          "type": "string",
          "format": "uri"
        },
        "website": {
          "type": "string",
          "format": "uri"
        }
      },
      "required": ["fullName", "email"]
    },
    "objective": {
      "title": "Objective",
      "$comment": "Candidate Objective",
      "description": "Candidate Career Objective",
      "type": "string"
    },
    "skills": {
      "title": "Skills",
      "$comment": "List of Candidate Skills",
      "description": "List of Candidate Skills",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "languages": {
      "title": "Languages",
      "$comment": "List of Candidate Spoken Languages",
      "description": "List of Candidate Spoken Languages",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "language": {
            "type": "string"
          },
          "proficiency": {
            "type": "string",
            "enum": ["Basic", "Conversational", "Fluent", "Native"]
          }
        },
        "required": ["language", "proficiency"]
      }
    },
    "education": {
      "title": "Education",
      "$comment": "List of Candidate Education",
      "description": "List of Candidate Education",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "institution": {
            "type": "string"
          },
          "degree": {
            "type": "string"
          },
          "major": {
            "type": "string"
          },
          "startDate": {
            "type": "string",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "format": "date"
          },
          "GPA": {
            "type": "number"
          }
        },
        "required": ["institution", "degree", "startDate", "endDate"]
      }
    },
    "workExperience": {
      "title": "Work Experience",
      "$comment": "List of Candidate Work Experience",
      "description": "List of Candidate Work Experience",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "company": {
            "type": "string"
          },
          "position": {
            "type": "string"
          },
          "location": {
            "type": "string"
          },
          "startDate": {
            "type": "string",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "format": "date"
          },
          "responsibilities": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        },
        "required": ["company", "position", "startDate", "endDate"]
      }
    },
    "certifications": {
      "title": "Certifications",
      "$comment": "List of Candidate Certifications",
      "description": "List of Candidate Certifications",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "organization": {
            "type": "string"
          },
          "dateReceived": {
            "type": "string",
            "format": "date"
          },
          "validUntil": {
            "type": "string",
            "format": "date"
          }
        },
        "required": ["name", "organization", "dateReceived"]
      }
    },
    "projects": {
      "title": "Projects",
      "$comment": "List of Candidate Projects",
      "description": "List of Candidate Projects",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "technologiesUsed": {
            "type": "array",
            "items": {
              "type": "string"
            }
          },
          "startDate": {
            "type": "string",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "format": "date"
          },
          "projectURL": {
            "type": "string",
            "format": "uri"
          }
        },
        "required": [
          "title",
          "description",
          "technologiesUsed",
          "startDate",
          "endDate"
        ]
      }
    },
    "volunteerExperience": {
      "title": "Volunteer Experience",
      "$comment": "List of Candidate Volunteer Experience",
      "description": "List of Candidate Volunteer Experience",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "organization": {
            "type": "string"
          },
          "position": {
            "type": "string"
          },
          "location": {
            "type": "string"
          },
          "startDate": {
            "type": "string",
            "format": "date"
          },
          "endDate": {
            "type": "string",
            "format": "date"
          },
          "activities": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        },
        "required": ["organization", "position", "startDate", "endDate"]
      }
    },
    "awards": {
      "title": "Awards",
      "$comment": "List of Candidate Awards",
      "description": "List of Candidate Awards",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string"
          },
          "organization": {
            "type": "string"
          },
          "dateReceived": {
            "type": "string",
            "format": "date"
          },
          "description": {
            "type": "string"
          }
        },
        "required": ["title", "organization", "dateReceived"]
      }
    },
    "publications": {
      "title": "Publications",
      "$comment": "List of Candidate Publications",
      "description": "List of Candidate Publications",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string"
          },
          "publisher": {
            "type": "string"
          },
          "publicationDate": {
            "type": "string",
            "format": "date"
          },
          "url": {
            "type": "string",
            "format": "uri"
          }
        },
        "required": ["title", "publisher", "publicationDate"]
      }
    },
    "references": {
      "title": "References",
      "$comment": "List of Candidate References",
      "description": "List of Candidate References",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "title": {
            "type": "string"
          },
          "company": {
            "type": "string"
          },
          "phone": {
            "type": "string"
          },
          "email": {
            "type": "string",
            "format": "email"
          }
        },
        "required": ["name", "title", "company", "phone", "email"]
      }
    },
    "interests": {
      "title": "Interests",
      "$comment": "List of Candidate Interests",
      "description": "List of Candidate Interests",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "additionalInformation": {
      "title": "Additional Information",
      "$comment": "Additional Information",
      "description": "Additional Information",
      "type": "object",
      "properties": {
        "hobbies": {
          "title": "Hobbies",
          "$comment": "List of Candidate Hobbies",
          "description": "List of Candidate Hobbies",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "memberships": {
          "title": "Memberships",
          "$comment": "List of Candidate Memberships",
          "description": "List of Candidate Memberships",
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "organization": {
                "type": "string"
              },
              "position": {
                "type": "string"
              },
              "startDate": {
                "type": "string",
                "format": "date"
              },
              "endDate": {
                "type": "string",
                "format": "date"
              }
            },
            "required": ["organization", "position", "startDate", "endDate"]
          }
        }
      }
    }
  },
  "required": ["personalInformation"],
  "definitions": {
    "objects": {},
    "types": {}
  }
}
