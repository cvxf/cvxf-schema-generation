{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Resume",
  "type": "object",
  "properties": {
    "personalInformation": {
      "$ref": "#/definitions/objects/personalInformation"
    },
    "objective": {
      "$ref": "#/definitions/types/objectiveType"
    },
    "skills": {
      "$ref": "#/definitions/objects/skills"
    },
    "languages": {
      "$ref": "#/definitions/objects/languages"
    },
    "education": {
      "$ref": "#/definitions/objects/education"
    },
    "workExperience": {
      "$ref": "#/definitions/objects/workExperience"
    },
    "certifications": {
      "$ref": "#/definitions/objects/certifications"
    },
    "projects": {
      "$ref": "#/definitions/objects/projects"
    },
    "volunteerExperience": {
      "$ref": "#/definitions/objects/volunteerExperience"
    },
    "awards": {
      "$ref": "#/definitions/objects/awards"
    },
    "publications": {
      "$ref": "#/definitions/objects/publications"
    },
    "references": {
      "$ref": "#/definitions/objects/references"
    },
    "interests": {
      "$ref": "#/definitions/objects/interests"
    },
    "additionalInformation": {
      "$ref": "#/definitions/objects/additionalInformation"
    }
  },
  "required": [
    "personalInformation"
  ],
  "definitions": {
    "objects": {
      "personalInformation": {
        "$comment": "Personal Information",
        "title": "Personal Information",
        "description": "Stores the candidate's personal information",
        "type": "object",
        "properties": {
          "fullName": {
            "$ref": "#/definitions/types/fullNameType"
          },
          "email": {
            "$ref": "#/definitions/types/emailType"
          },
          "phone": {
            "$ref": "#/definitions/types/phoneType"
          },
          "address": {
            "$ref": "#/definitions/types/addressType"
          },
          "linkedInProfile": {
            "$ref": "#/definitions/types/linkedInProfileType"
          },
          "website": {
            "$ref": "#/definitions/types/websiteType"
          }
        },
        "required": [
          "fullName",
          "email"
        ]
      },
      "skills": {
        "title": "Skills",
        "$comment": "List of Candidate Skills",
        "description": "List of Candidate Skills",
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/skillsItemsType"
        }
      },
      "languagesItems": {
        "type": "object",
        "properties": {
          "language": {
            "$ref": "#/definitions/types/languageType"
          },
          "proficiency": {
            "$ref": "#/definitions/types/proficiencyType"
          }
        },
        "required": [
          "language",
          "proficiency"
        ]
      },
      "languages": {
        "title": "Languages",
        "$comment": "List of Candidate Spoken Languages",
        "description": "List of Candidate Spoken Languages",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/languagesItems"
        }
      },
      "educationItems": {
        "type": "object",
        "properties": {
          "institution": {
            "$ref": "#/definitions/types/institutionType"
          },
          "degree": {
            "$ref": "#/definitions/types/degreeType"
          },
          "major": {
            "$ref": "#/definitions/types/majorType"
          },
          "startDate": {
            "$ref": "#/definitions/types/startDateType"
          },
          "endDate": {
            "$ref": "#/definitions/types/endDateType"
          },
          "GPA": {
            "$ref": "#/definitions/types/GPAType"
          }
        },
        "required": [
          "institution",
          "degree",
          "startDate",
          "endDate"
        ]
      },
      "education": {
        "title": "Education",
        "$comment": "List of Candidate Education",
        "description": "List of Candidate Education",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/educationItems"
        }
      },
      "responsibilities": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/responsibilitiesItemsType"
        }
      },
      "workExperienceItems": {
        "type": "object",
        "properties": {
          "company": {
            "$ref": "#/definitions/types/companyType"
          },
          "position": {
            "$ref": "#/definitions/types/positionType"
          },
          "location": {
            "$ref": "#/definitions/types/locationType"
          },
          "startDate": {
            "$ref": "#/definitions/types/startDateType"
          },
          "endDate": {
            "$ref": "#/definitions/types/endDateType"
          },
          "responsibilities": {
            "$ref": "#/definitions/objects/responsibilities"
          }
        },
        "required": [
          "company",
          "position",
          "startDate",
          "endDate"
        ]
      },
      "workExperience": {
        "title": "Work Experience",
        "$comment": "List of Candidate Work Experience",
        "description": "List of Candidate Work Experience",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/workExperienceItems"
        }
      },
      "certificationsItems": {
        "type": "object",
        "properties": {
          "name": {
            "$ref": "#/definitions/types/nameType"
          },
          "organization": {
            "$ref": "#/definitions/types/organizationType"
          },
          "dateReceived": {
            "$ref": "#/definitions/types/dateReceivedType"
          },
          "validUntil": {
            "$ref": "#/definitions/types/validUntilType"
          }
        },
        "required": [
          "name",
          "organization",
          "dateReceived"
        ]
      },
      "certifications": {
        "title": "Certifications",
        "$comment": "List of Candidate Certifications",
        "description": "List of Candidate Certifications",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/certificationsItems"
        }
      },
      "technologiesUsed": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/technologiesUsedItemsType"
        }
      },
      "projectsItems": {
        "type": "object",
        "properties": {
          "title": {
            "$ref": "#/definitions/types/titleType"
          },
          "description": {
            "$ref": "#/definitions/types/descriptionType"
          },
          "technologiesUsed": {
            "$ref": "#/definitions/objects/technologiesUsed"
          },
          "startDate": {
            "$ref": "#/definitions/types/startDateType"
          },
          "endDate": {
            "$ref": "#/definitions/types/endDateType"
          },
          "projectURL": {
            "$ref": "#/definitions/types/projectURLType"
          }
        },
        "required": [
          "title",
          "description",
          "technologiesUsed",
          "startDate",
          "endDate"
        ]
      },
      "projects": {
        "title": "Projects",
        "$comment": "List of Candidate Projects",
        "description": "List of Candidate Projects",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/projectsItems"
        }
      },
      "activities": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/activitiesItemsType"
        }
      },
      "volunteerExperienceItems": {
        "type": "object",
        "properties": {
          "organization": {
            "$ref": "#/definitions/types/organizationType"
          },
          "position": {
            "$ref": "#/definitions/types/positionType"
          },
          "location": {
            "$ref": "#/definitions/types/locationType"
          },
          "startDate": {
            "$ref": "#/definitions/types/startDateType"
          },
          "endDate": {
            "$ref": "#/definitions/types/endDateType"
          },
          "activities": {
            "$ref": "#/definitions/objects/activities"
          }
        },
        "required": [
          "organization",
          "position",
          "startDate",
          "endDate"
        ]
      },
      "volunteerExperience": {
        "title": "Volunteer Experience",
        "$comment": "List of Candidate Volunteer Experience",
        "description": "List of Candidate Volunteer Experience",
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/volunteerExperienceItems"
        }
      },
      "awardsItems": {
        "type": "object",
        "properties": {
          "title": {
            "$ref": "#/definitions/types/titleType"
          },
          "organization": {
            "$ref": "#/definitions/types/organizationType"
          },
          "dateReceived": {
            "$ref": "#/definitions/types/dateReceivedType"
          },
          "description": {
            "$ref": "#/definitions/types/descriptionType"
          }
        },
        "required": [
          "title",
          "organization",
          "dateReceived"
        ]
      },
      "awards": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/awardsItems"
        }
      },
      "publicationsItems": {
        "type": "object",
        "properties": {
          "title": {
            "$ref": "#/definitions/types/titleType"
          },
          "publisher": {
            "$ref": "#/definitions/types/publisherType"
          },
          "publicationDate": {
            "$ref": "#/definitions/types/publicationDateType"
          },
          "url": {
            "$ref": "#/definitions/types/urlType"
          }
        },
        "required": [
          "title",
          "publisher",
          "publicationDate"
        ]
      },
      "publications": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/publicationsItems"
        }
      },
      "referencesItems": {
        "type": "object",
        "properties": {
          "name": {
            "$ref": "#/definitions/types/nameType"
          },
          "title": {
            "$ref": "#/definitions/types/titleType"
          },
          "company": {
            "$ref": "#/definitions/types/companyType"
          },
          "phone": {
            "$ref": "#/definitions/types/phoneType"
          },
          "email": {
            "$ref": "#/definitions/types/emailType"
          }
        },
        "required": [
          "name",
          "title",
          "company",
          "phone",
          "email"
        ]
      },
      "references": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/referencesItems"
        }
      },
      "interests": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/interestsItemsType"
        }
      },
      "hobbies": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/types/hobbiesItemsType"
        }
      },
      "membershipsItems": {
        "type": "object",
        "properties": {
          "organization": {
            "$ref": "#/definitions/types/organizationType"
          },
          "position": {
            "$ref": "#/definitions/types/positionType"
          },
          "startDate": {
            "$ref": "#/definitions/types/startDateType"
          },
          "endDate": {
            "$ref": "#/definitions/types/endDateType"
          }
        },
        "required": [
          "organization",
          "position",
          "startDate",
          "endDate"
        ]
      },
      "memberships": {
        "type": "array",
        "items": {
          "$ref": "#/definitions/objects/membershipsItems"
        }
      },
      "additionalInformation": {
        "type": "object",
        "properties": {
          "hobbies": {
            "$ref": "#/definitions/objects/hobbies"
          },
          "memberships": {
            "$ref": "#/definitions/objects/memberships"
          }
        }
      }
    },
    "types": {
      "fullNameType": {
        "type": "string"
      },
      "emailType": {
        "type": "string",
        "format": "email"
      },
      "phoneType": {
        "type": "string"
      },
      "addressType": {
        "type": "string"
      },
      "linkedInProfileType": {
        "type": "string",
        "format": "uri"
      },
      "websiteType": {
        "type": "string",
        "format": "uri"
      },
      "objectiveType": {
        "title": "Objective",
        "$comment": "Candidate Objective",
        "description": "Candidate Career Objective",
        "type": "string"
      },
      "skillsItemsType": {
        "type": "string"
      },
      "languageType": {
        "type": "string"
      },
      "proficiencyType": {
        "type": "string",
        "enum": [
          "Basic",
          "Conversational",
          "Fluent",
          "Native"
        ]
      },
      "institutionType": {
        "type": "string"
      },
      "degreeType": {
        "type": "string"
      },
      "majorType": {
        "type": "string"
      },
      "startDateType": {
        "type": "string",
        "format": "date"
      },
      "endDateType": {
        "type": "string",
        "format": "date"
      },
      "GPAType": {
        "type": "number"
      },
      "companyType": {
        "type": "string"
      },
      "positionType": {
        "type": "string"
      },
      "locationType": {
        "type": "string"
      },
      "responsibilitiesItemsType": {
        "type": "string"
      },
      "nameType": {
        "type": "string"
      },
      "organizationType": {
        "type": "string"
      },
      "dateReceivedType": {
        "type": "string",
        "format": "date"
      },
      "validUntilType": {
        "type": "string",
        "format": "date"
      },
      "titleType": {
        "type": "string"
      },
      "descriptionType": {
        "type": "string"
      },
      "technologiesUsedItemsType": {
        "type": "string"
      },
      "projectURLType": {
        "type": "string",
        "format": "uri"
      },
      "activitiesItemsType": {
        "type": "string"
      },
      "publisherType": {
        "type": "string"
      },
      "publicationDateType": {
        "type": "string",
        "format": "date"
      },
      "urlType": {
        "type": "string",
        "format": "uri"
      },
      "interestsItemsType": {
        "type": "string"
      },
      "hobbiesItemsType": {
        "type": "string"
      }
    }
  }
}