import * as fs from "fs";

const INPUT_JSON_FILE = "./assets/json/schema/cvxf_simplified.schema.json";
const OUTPUT_JSON_FILE = "./assets/json/schema/cvxf.schema.json";
const PATH_SEP = "/";
const ITEMS = "items";

function readJsonFile(filePath: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
}

function saveJsonFile(filePath: string, data: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, "utf8", (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
}

interface RecursiveMapFunction {
  (key: string, value: any, parent: any, typename: string, root: any): any;
}

function recursiveMap(
  obj: any,
  fn: RecursiveMapFunction,
  path: string = "",
  key: string = "",
  parent: any = undefined,
  root: any = obj
): any {
  var result = obj;
  var typename = typeof obj;
  if (Array.isArray(obj)) {
    var index = 0;
    result = [];
    obj.forEach((item) => {
      result.push(recursiveMap(item, fn, path, String(index++), obj, root));
    });
  } else if (typeof obj === "object" && obj !== null) {
    result = {};
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const value = obj[key];
        result[key] = recursiveMap(
          value,
          fn,
          `${path}${PATH_SEP}${key}`,
          key,
          obj,
          root
        );
      }
    }
  }

  return fn(path, result, parent, typename, root);
}

function getDefinitionsRoot(root: any): any {
  if (!root.definitions) {
    root.definitions = {};
  }
  return root.definitions;
}

function getTypeDefinitionsRoot(root: any): any {
  const definitions = getDefinitionsRoot(root);
  if (!definitions.types) {
    definitions.types = {};
  }
  return definitions.types;
}

function getObjectDefinitionsRoot(root: any): any {
  const definitions = getDefinitionsRoot(root);
  if (!definitions.objects) {
    definitions.objects = {};
  }
  return definitions.objects;
}

function deQualify(qualified: string): string {
  return qualified.lastIndexOf(PATH_SEP)
    ? qualified.substring(qualified.lastIndexOf(PATH_SEP) + 1)
    : qualified;
}

function getItemsParentName(qualified: string): string {
  return qualified.endsWith(`/${ITEMS}`)
    ? deQualify(qualified.substring(0, qualified.lastIndexOf(`/${ITEMS}`)))
    : deQualify(qualified);
}

function handleObjectType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  //console.log("handle object type", key);
  var baseName = deQualify(key);
  if (baseName === "") {
    return value;
  }

  if (baseName === ITEMS) {
    baseName = `${getItemsParentName(key)}Items`;
  }

  const types = getObjectDefinitionsRoot(root);

  types[`${baseName}`] = value;
  return {
    $ref: `#/definitions/objects/${baseName}`,
  };
}

function handleArrayType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  var baseName = deQualify(key);

  const types = getObjectDefinitionsRoot(root);

  types[`${baseName}`] = value;
  return {
    $ref: `#/definitions/objects/${baseName}`,
  };
}

function handlePrimativeType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  const types = getTypeDefinitionsRoot(root);

  var baseName = deQualify(key);

  if (baseName === ITEMS) {
    baseName = `${getItemsParentName(key)}Items`;
  }

  types[`${baseName}Type`] = value;
  return {
    $ref: `#/definitions/types/${baseName}Type`,
  };
}

const objectHandlers: { [key: string]: RecursiveMapFunction } = {
  string: handlePrimativeType,
  boolean: handlePrimativeType,
  number: handlePrimativeType,
  object: handleObjectType,
  array: handleArrayType,
};

function handleObject(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  const handler = objectHandlers[value.type];
  if (handler) return handler(key, value, parent, typename, root);

  return value;
}

function handleArray(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  //console.log(key);
  return value;
}

function handlePrimitave(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  //console.log(key, value);
  return value;
}

const handlers: { [key: string]: RecursiveMapFunction } = {
  object: handleObject,
  array: handleArray,
  boolean: handlePrimitave,
  string: handlePrimitave,
  number: handlePrimitave,
};

async function main() {
  const jsonFilePath = INPUT_JSON_FILE;
  try {
    const jsonContent = await readJsonFile(jsonFilePath);

    const mappedJson = recursiveMap(
      jsonContent,
      (key, value, parent, typename, root) => {
        //console.log(`key ${key}`);
        if (key.startsWith("/definitions")) return value;
        return handlers[typename](key, value, parent, typename, root);
      }
    );

    console.log("Mapped JSON:");
    const output = JSON.stringify(mappedJson, null, 2);

    await saveJsonFile(OUTPUT_JSON_FILE, output);
  } catch (error) {
    console.error("Error:", error);
  }
}

main();
